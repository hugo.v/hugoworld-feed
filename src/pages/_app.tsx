import React, { ReactElement } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import type { AppProps } from "next/app";
import Router from "next/router";
import Head from "next/head";
import NProgress from "nprogress";

import Header from "../components/molecules/Header/Header";
import Footer from "../components/molecules/Footer/Footer";

import "bootstrap/dist/css/bootstrap.css";
import "../styles/globals.scss";

if (!process.browser) React.useLayoutEffect = React.useEffect;

NProgress.configure({ showSpinner: false });

Router.events.on("routeChangeStart", () => {
  NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

const queryClient = new QueryClient();

const App = ({ Component, pageProps }: AppProps): ReactElement => {
  return (
    <>
      <Head>
        <title>Hugoworld Feed</title>
      </Head>
      <QueryClientProvider client={queryClient}>
        <div className="container">
          <Header />
          <div className="flex-grow-1 flex-shrink-1">
            <Component {...pageProps} />
          </div>
          <Footer />
        </div>
      </QueryClientProvider>
    </>
  );
};

export default App;
