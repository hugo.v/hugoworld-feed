import type { NextPage } from "next";
import Head from "next/head";

import styles from "./index.module.scss";

const Page404: NextPage = () => {
  return (
    <>
      <Head>
        <title>404 - Hugoworld Feed</title>
      </Head>
      <style jsx global>
        {`
          body {
            height: 100vh;
          }
          #__next {
            height: 100%;
          }
          .container {
            height: 100%;
            display: flex;
            flex-direction: column;
          }
        `}
      </style>
      <div className={styles.page404}>
        <div className={styles.page404Header}>404</div>
      </div>
    </>
  );
};

export default Page404;
