import type { NextPage, GetServerSideProps } from "next";

// Services
import { getArticles } from "../services/articleService";

// Components
import Day from "../components/molecules/Day/Day";
interface HomeProps {
  articles: Article[];
  date: string;
}

const Home: NextPage<HomeProps> = ({ articles, date }) => {
  return (
    <>
      <Day date={date} articles={articles} />
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const today = new Date().toISOString().split("T")[0];
  const { data: dailyArticles } = await getArticles({
    date: today,
  });

  return {
    props: {
      articles: dailyArticles,
      date: today,
    },
  };
};

export default Home;
