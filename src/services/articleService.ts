import { AxiosResponse } from "axios";
import api, { publicApi } from "./api";

/**
 * GET /articles
 */
interface ArticlesOptions {
  date?: string;
  search?: string;
  last?: number;
}
type ArticlesResponse = Promise<AxiosResponse<Article[]>>;
export const getArticles = (options: ArticlesOptions): ArticlesResponse => {
  return publicApi.get<Article[]>("articles", { params: options });
};

/**
 * GET /article
 */
type ArticleResponse = Promise<AxiosResponse<Article>>;
export const getArticle = (id: string | string[]): ArticleResponse => {
  return api.get<Article>(`articles/${id}`);
};
