import type { NextPage, GetStaticProps, GetStaticPaths } from "next";
import Head from "next/head";

// Services
import { getArticles } from "../../services/articleService";

// Components
import Day from "../../components/molecules/Day/Day";

// Utils
import { getFormattedDate, getCurrentMonthDays } from "../../utils/date";

type DayProps = {
  articles: Article[];
  day: string;
};

const DayPage: NextPage<DayProps> = ({ articles, day }) => {
  return (
    <>
      <Head>
        <title>{`${getFormattedDate(day)} - Hugoworld Feed`}</title>
      </Head>
      <Day date={day} articles={articles} />
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: getCurrentMonthDays().map((day) => ({
      params: {
        day,
      },
    })),
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  if (params?.day && typeof params.day === "string") {
    const { data: articles } = await getArticles({ date: params.day });
    return {
      props: {
        articles,
        day: params.day,
      },
    };
  }
  return {
    notFound: true,
  };
};

export default DayPage;
