import axios from "axios";

const api = axios.create({
  baseURL: `https://${process.env.NEXT_PUBLIC_API_URL}`,
  headers: {
    "Content-Type": "application/json",
  },
  auth: {
    username: "admin",
    password: process.env.API_PSW || "",
  },
});

export const publicApi = axios.create({
  baseURL: `https://${process.env.NEXT_PUBLIC_API_URL}`,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

export default api;
