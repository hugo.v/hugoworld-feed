type Article = {
  _id: string;
  title: string;
  content: string;
  date: string;
  url: string;
  id: string;
  source: string;
};
