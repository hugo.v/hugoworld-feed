import { useMutation } from "react-query";
import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { formatISO } from "date-fns";
import frLocale from "date-fns/locale/fr";
import DatePicker, { registerLocale } from "react-datepicker";
import { useFormik } from "formik";

// Components
import Trail from "../../components/molecules/Trail/Trail";

// Services
import { getArticles } from "../../services/articleService";

// Utils
import { getReadingTime } from "../../utils/article";
import { getFormattedDate } from "../../utils/date";

import styles from "./search.module.scss";

registerLocale("fr", frLocale);

const Search: NextPage = () => {
  const { push } = useRouter();
  const {
    data: searchedArticles,
    mutate: searchArticles,
    isLoading: isSearchLoading,
  } = useMutation("getArticles", (search: string) => getArticles({ search }));
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: (submitValues) => {
      if (submitValues.search) searchArticles(submitValues.search);
    },
  });
  const { values, handleSubmit, getFieldProps } = formik;

  return (
    <>
      <Head>
        <title>{`Rechercher - ${process.env.NEXT_PUBLIC_APP_NAME}`}</title>
      </Head>
      <div className={styles.search}>
        <label htmlFor="search" className={`form-label ${styles.label}`}>
          Recherche par date
        </label>
        <div className={styles.dateInput}>
          <DatePicker
            selected={undefined}
            showPreviousMonths={false}
            locale="fr"
            onChange={(date) => {
              if (date instanceof Date) {
                push(
                  `/day/${formatISO(new Date(date), {
                    representation: "date",
                  })}`
                );
              }
            }}
            inline
          />
        </div>
      </div>
      <div className={styles.search}>
        <form onSubmit={handleSubmit}>
          <label htmlFor="search" className={`form-label ${styles.label}`}>
            Recherche par titre ou contenu
          </label>
          <input
            autoComplete="off"
            className={`form-control ${styles.input}`}
            placeholder="..."
            {...getFieldProps("search")}
          />
          <span
            onClick={() =>
              values.search ? searchArticles(values.search) : null
            }
            role="button"
            tabIndex={0}
            onKeyDown={undefined}
            className={`${styles.inputIcon} ${
              values.search ? "" : styles.hidden
            }`}
          >
            <svg
              viewBox="0 0 24 24"
              aria-hidden="true"
              className={styles.searchLogo}
            >
              <g>
                <path d="M21.53 20.47l-3.66-3.66C19.195 15.24 20 13.214 20 11c0-4.97-4.03-9-9-9s-9 4.03-9 9 4.03 9 9 9c2.215 0 4.24-.804 5.808-2.13l3.66 3.66c.147.146.34.22.53.22s.385-.073.53-.22c.295-.293.295-.767.002-1.06zM3.5 11c0-4.135 3.365-7.5 7.5-7.5s7.5 3.365 7.5 7.5-3.365 7.5-7.5 7.5-7.5-3.365-7.5-7.5z"></path>
              </g>
            </svg>
          </span>
        </form>
        <div className={styles.results}>
          {isSearchLoading && (
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          )}
          <div className="row">
            <Trail>
              {searchedArticles?.data
                .filter((a, i) => i < 50)
                .sort(
                  (a, b) =>
                    new Date(b.date).getTime() - new Date(a.date).getTime()
                )
                .map((article) => {
                  const TITLE_LENGTH = 80;
                  return (
                    <div key={article.id}>
                      <Link passHref href={`/article/${article.id}`}>
                        <div className={`${styles.result}`}>
                          <p className={styles.date}>
                            {getFormattedDate(article.date)}
                          </p>
                          <div>
                            {article.title.length > TITLE_LENGTH
                              ? `${article.title.substring(0, TITLE_LENGTH)}...`
                              : article.title}
                          </div>
                          <div className={styles.description}>
                            <span />
                            <p>{`Temps de lecture: ${getReadingTime(
                              article.content
                            )} min`}</p>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                }) || []}
            </Trail>
          </div>
        </div>
      </div>
    </>
  );
};

export default Search;
