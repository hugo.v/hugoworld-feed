import React, {
  useState,
  useMemo,
  ReactElement,
  FunctionComponent,
} from "react";
import Link from "next/link";
import { isToday, isFuture, addHours } from "date-fns";

// Hooks
import useInterval from "../../../hooks/useInterval";

// Utils
import { getFormattedDate, getCountdown } from "../../../utils/date";
import { getReadingTime } from "../../../utils/article";

import styles from "./Day.module.scss";

interface DayProps {
  articles: Article[];
  date: string;
}

const Day: FunctionComponent<DayProps> = ({ articles, date }): ReactElement => {
  const isTurfu = useMemo(() => isFuture(addHours(new Date(date), 8)), [date]);
  const is2Day = useMemo(() => isToday(new Date(date)), [date]);
  const futureDate = useMemo(
    () => (isTurfu ? addHours(new Date(date), 8) : null),
    [isTurfu, date]
  );

  const [countdown, setCountdown] = useState<string>(
    getCountdown(new Date(), futureDate)
  );

  useInterval(() => {
    setCountdown(getCountdown(new Date(), futureDate));
  }, 1000);

  return (
    <div>
      <h1 className={styles.title}>{getFormattedDate(date)}</h1>
      {isTurfu && (
        <div className={styles.turfu}>
          <div>
            {is2Day ? "Du matin ?" : "Déjà dans le turfu ?"}{" "}
            <span role="img" aria-label="early">
              {is2Day ? "🥐 ☕" : "⏱️"}
            </span>
            <br />
            {`Les articles ${
              is2Day ? "d'aujourd'hui" : "pour ce jour"
            } seront disponibles dans environ:`}
          </div>
          <div className={styles.countdown}>{countdown}</div>
        </div>
      )}
      <ul className={styles.articles}>
        {articles.map((article) => (
          <Link passHref key={article.id} href={`/article/${article.id}`}>
            <li className={styles.article}>
              <h5>{article.title}</h5>
              <div className={styles.description}>
                <span />
                <p>{`Temps de lecture: ${getReadingTime(
                  article.content
                )} min`}</p>
              </div>
            </li>
          </Link>
        ))}
      </ul>
    </div>
  );
};

export default Day;
