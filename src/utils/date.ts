import {
  format,
  formatISO,
  startOfMonth,
  eachDayOfInterval,
  formatDuration,
  intervalToDuration,
} from "date-fns";
import frLocale from "date-fns/locale/fr";

export const getFormattedDate = (date: string): string =>
  format(new Date(date), "dd MMMM yyyy", {
    locale: frLocale,
  });

export const getCurrentMonthDays = (): string[] => {
  const today = new Date();
  const start = startOfMonth(today);
  return eachDayOfInterval({
    start,
    end: today,
  }).map((day) => formatISO(day, { representation: "date" }));
};

export const getCountdown = (
  startDate: Date,
  futureDate: Date | null
): string => {
  if (futureDate instanceof Date) {
    const duration = intervalToDuration({
      start: startDate,
      end: futureDate,
    });

    return formatDuration(duration, {
      delimiter: ", ",
      locale: frLocale,
    });
  }
  return "Une erreur est survenue pendant l'affichage du compteur";
};
