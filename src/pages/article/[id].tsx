import type { NextPage, GetStaticProps, GetStaticPaths } from "next";
import Head from "next/head";
import Image from "next/image";

// Services
import { getArticle, getArticles } from "../../services/articleService";

// Utils
import { getFormattedDate } from "../../utils/date";

import styles from "./article.module.scss";

type ArticleProps = {
  article: Article;
};

const Article: NextPage<ArticleProps> = ({ article }) => {
  return (
    <>
      <Head>
        <title>{`${article.title} - Hugoworld Feed`}</title>
      </Head>
      <div className={styles.articleContainer}>
        <div className="row mb-3">
          <div className="col">
            <div className="d-inline-block">
              <h3 className={styles.title}>{article.title}</h3>
            </div>
          </div>
        </div>
        <div className="row mb-4">
          <div className="col d-flex justify-content-between align-items-center text-color-grey-500">
            <span className="text-color-grey-500">
              {getFormattedDate(article.date)}
            </span>
            <a
              className="me-2"
              target="_blank"
              href={article.url}
              rel="noreferrer"
            >
              <Image height={20} width={20} src="/images/rt-logo.png" />
            </a>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <p
              className={`mb-0 fw-light lh-base text-justify ${styles.content}`}
            >
              {article.content}
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const { data: articles } = await getArticles({ last: 250 });

  return {
    paths: articles.map((article: Article) => ({
      params: { id: article.id },
    })),
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  if (params?.id) {
    const { data: article } = await getArticle(params.id);

    if (!article) return { notFound: true };
    return {
      props: {
        article,
      },
      revalidate: 60,
    };
  }
  return {
    notFound: true,
  };
};

export default Article;
